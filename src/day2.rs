fn parse_game(game: &str) -> (u32, u32, u32) {
    let (mut r, mut g, mut b) = (0u32, 0u32, 0u32);
    game.split(',').for_each(|color| {
        let (n, color_name) = color.trim().split_once(' ').expect("Bad color!");
        let number = n.parse::<u32>().expect("bad color number!");
        match color_name {
            "red" => r = number,
            "green" => g = number,
            "blue" => b = number,
            &_ => println!("bad color name: {}", color_name),
        }
    });
    (r, g, b)
}

fn get_max_rgb(games: &str) -> (u32, u32, u32) {
    games
        .split(';')
        .fold((0u32, 0u32, 0u32), |(r_acc, g_acc, b_acc), game| {
            let (r, g, b) = parse_game(game);
            (r_acc.max(r), g_acc.max(g), b_acc.max(b))
        })
}

fn parse_round(round: &str) -> (u32, u32, u32, u32) {
    let (gameid, games) = round.split_once(':').expect("Colon not found!");
    let (_, id_str) = gameid.split_once(' ').expect("space not fount in gameid!");
    let id = id_str
        .parse::<u32>()
        .expect("Could not convert game id to u32!");
    let (r, g, b) = get_max_rgb(games);
    (id, r, g, b)
}

fn cube_game_round(round: &str) -> u32 {
    let (id, red, green, blue) = parse_round(round);
    if red > 12 || green > 13 || blue > 14 {
        return 0;
    }
    id
}

fn cube_power_game_round(round: &str) -> u32 {
    let (_, red, green, blue) = parse_round(round);
    red * green * blue
}

pub fn cube_game(v: &[String]) -> u32 {
    v.iter().fold(0, |acc, round| acc + cube_game_round(round))
}

pub fn cube_power_game(v: &[String]) -> u32 {
    v.iter()
        .fold(0, |acc, round| acc + cube_power_game_round(round))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::filereader::{filelines_to_vector, read_filelines};

    #[test]
    fn test_cube_game() {
        let input = [
            String::from("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"),
            String::from("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue"),
            String::from(
                "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
            ),
            String::from(
                "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
            ),
            String::from("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"),
        ];
        let result = cube_game(&input);
        assert_eq!(result, 8);
    }

    #[test]
    fn test_cube_game_from_file() {
        let filelines = read_filelines("./data/day2.txt").expect("Failed reading lines, day2");
        let input = filelines_to_vector::<String>(filelines);
        let result = cube_game(&input);
        assert_eq!(result, 2879);
    }

    #[test]
    fn test_cube_power_game() {
        let input = [
            String::from("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"),
            String::from("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue"),
            String::from(
                "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
            ),
            String::from(
                "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
            ),
            String::from("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"),
        ];
        let result = cube_power_game(&input);
        assert_eq!(result, 2286);
    }

    #[test]
    fn test_cube_power_game_from_file() {
        let filelines = read_filelines("./data/day2.txt").expect("Failed reading lines, day2");
        let input = filelines_to_vector::<String>(filelines);
        let result = cube_power_game(&input);
        assert_eq!(result, 65122);
    }
}
