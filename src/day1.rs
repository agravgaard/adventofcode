fn index_to_digit(s: &String, index: usize) -> u32 {
    (s.as_bytes()[index] as char)
        .to_digit(10)
        .expect("Could not convert char to digit!")
}

fn decode_single(code: &String) -> u32 {
    // Assume at least one number is garanteed to be in the string
    let first_index = code.find(|c: char| c.is_ascii_digit()).unwrap();
    let last_index = code.rfind(|c: char| c.is_ascii_digit()).unwrap();
    index_to_digit(code, first_index) * 10 + index_to_digit(code, last_index)
}

const SPELLED_NUMBERS: [&str; 9] = [
    "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
];

fn spelled_to_digit(s: &str) -> u32 {
    let index = SPELLED_NUMBERS.iter().position(|num| *num == s);
    if let Some(value) = index {
        return 1 + value as u32;
    }
    0
}

fn get_first_digit_spelled(s: &str) -> (usize, u32) {
    let no_match = (s.len(), "zero");
    let matches: Vec<(usize, &str)> = SPELLED_NUMBERS
        .iter()
        .map(|number| -> (usize, &str) {
            let mut m_iter = s.match_indices(number);
            m_iter.next().unwrap_or(no_match)
        })
        .collect();
    let first_match = matches.iter().min_by_key(|m| m.0).unwrap_or(&no_match);
    (first_match.0, spelled_to_digit(first_match.1))
}

fn get_last_digit_spelled(s: &str) -> (usize, u32) {
    let no_match = (0, "zero");
    let matches: Vec<(usize, &str)> = SPELLED_NUMBERS
        .iter()
        .map(|number| -> (usize, &str) {
            let mut m_iter = s.rmatch_indices(number);
            m_iter.next().unwrap_or(no_match)
        })
        .collect();
    let last_match = matches.iter().max_by_key(|m| m.0).unwrap_or(&no_match);
    (last_match.0, spelled_to_digit(last_match.1))
}

fn decode_spelled_single(code: &String) -> u32 {
    // Assume at least one number is garanteed to be in the string
    let first_index = code.find(|c: char| c.is_ascii_digit()).unwrap();
    let last_index = code.rfind(|c: char| c.is_ascii_digit()).unwrap();

    let (first_index_spelled, first_spelled) = get_first_digit_spelled(code);
    let (last_index_spelled, last_spelled) = get_last_digit_spelled(code);

    let mut first = first_spelled;
    if first_index <= first_index_spelled {
        first = index_to_digit(code, first_index);
    }
    let mut last = last_spelled;
    if last_index >= last_index_spelled {
        last = index_to_digit(code, last_index);
    }
    first * 10 + last
}

pub fn decode(v: &[String]) -> u32 {
    v.iter().fold(0, |acc, code| acc + decode_single(code))
}

pub fn decode_spelled(v: &[String]) -> u32 {
    v.iter()
        .fold(0, |acc, code| acc + decode_spelled_single(code))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::filereader::{filelines_to_vector, read_filelines};

    #[test]
    fn test_decode_single() {
        let test_code1 = String::from("1abc2");
        assert_eq!(12, decode_single(&test_code1));
        let test_code2 = String::from("pqr3stu8vwx");
        assert_eq!(38, decode_single(&test_code2));
        let test_code3 = String::from("a1b2c3d4e5f");
        assert_eq!(15, decode_single(&test_code3));
        let test_code4 = String::from("treb7uchet");
        assert_eq!(77, decode_single(&test_code4));
    }

    #[test]
    fn test_decode() {
        let input = [
            String::from("1abc2"),
            String::from("pqr3stu8vwx"),
            String::from("a1b2c3d4e5f"),
            String::from("treb7uchet"),
        ];
        let result = decode(&input);
        assert_eq!(result, 142);
    }

    #[test]
    fn test_decode_from_file() {
        let filelines = read_filelines("./data/day1.txt").expect("Failed reading lines, day1");
        let input = filelines_to_vector::<String>(filelines);
        let result = decode(&input);
        assert_eq!(result, 54304);
    }

    #[test]
    fn test_spelled_to_digit() {
        SPELLED_NUMBERS.iter().enumerate().for_each(|(i, num)| {
            assert_eq!(spelled_to_digit(num), 1 + i as u32);
        });
        assert_eq!(spelled_to_digit("zero"), 0);
        assert_eq!(spelled_to_digit("five"), 5);
        assert_eq!(spelled_to_digit("something"), 0);
    }

    #[test]
    fn test_get_first_digit_spelled() {
        let input = [
            (String::from("1abc2"), (5, 0)),
            (String::from("pqr3stu8vwx"), (11, 0)),
            (String::from("a1b2c3d4e5f"), (11, 0)),
            (String::from("treb7uchet"), (10, 0)),
            (String::from("two1nine"), (0, 2)),
            (String::from("eightwo2three"), (0, 8)),
            (String::from("abcone2threexyz"), (3, 1)),
            (String::from("xtwone3four"), (1, 2)),
            (String::from("4nineeightseven2"), (1, 9)),
            (String::from("zoneight234"), (1, 1)),
            (String::from("7pqrstsixteen"), (6, 6)),
        ];
        input.iter().for_each(|(s, exp)| {
            assert_eq!(get_first_digit_spelled(s), *exp);
        });
    }

    #[test]
    fn test_get_last_digit_spelled() {
        let input = [
            (String::from("1abc2"), (0, 0)),
            (String::from("pqr3stu8vwx"), (0, 0)),
            (String::from("a1b2c3d4e5f"), (0, 0)),
            (String::from("treb7uchet"), (0, 0)),
            (String::from("two1nine"), (4, 9)),
            (String::from("eightwo2three"), (8, 3)),
            (String::from("abcone2threexyz"), (7, 3)),
            (String::from("xtwone3four"), (7, 4)),
            (String::from("4nineeightseven2"), (10, 7)),
            (String::from("zoneight234"), (3, 8)),
            (String::from("7pqrstsixteen"), (6, 6)),
        ];
        input.iter().for_each(|(s, exp)| {
            assert_eq!(get_last_digit_spelled(s), *exp);
        });
    }

    #[test]
    fn test_decode_spelled() {
        let input = [
            String::from("1abc2"),
            String::from("pqr3stu8vwx"),
            String::from("a1b2c3d4e5f"),
            String::from("treb7uchet"),
            String::from("two1nine"),
            String::from("eightwo2three"),
            String::from("abcone2threexyz"),
            String::from("xtwone3four"),
            String::from("4nineeightseven2"),
            String::from("zoneight234"),
            String::from("7pqrstsixteen"),
        ];
        let result = decode_spelled(&input);
        assert_eq!(result, 142 + 281);
    }

    #[test]
    fn test_decode_spelled_from_file() {
        let filelines = read_filelines("./data/day1.txt").expect("Failed reading lines, day1");
        let input = filelines_to_vector::<String>(filelines);
        let result = decode_spelled(&input);
        assert_eq!(result, 54418);
    }
}
