//! A library for the adventofcode challenges

#![doc(html_root_url = "https://gitlab.com/agravgaard/adventofcode")]
#![forbid(unsafe_code)]
#![deny(
    absolute_paths_not_starting_with_crate,
    elided_lifetimes_in_paths,
    explicit_outlives_requirements,
    keyword_idents,
    macro_use_extern_crate,
    meta_variable_misuse,
    missing_abi,
    missing_copy_implementations,
    missing_debug_implementations,
    non_ascii_idents,
    noop_method_call,
    pointer_structural_match,
    single_use_lifetimes,
    trivial_casts,
    trivial_numeric_casts,
    unreachable_pub,
    unsafe_op_in_unsafe_fn,
    unstable_features,
    unused_crate_dependencies,
    unused_extern_crates,
    unused_import_braces,
    unused_lifetimes,
    unused_qualifications,
    unused_results,
    variant_size_differences
)]
#![warn(
    clippy::panic,
    future_incompatible,
    nonstandard_style,
    rust_2018_idioms
)]

pub mod day1;
pub mod day2;
pub mod day3;
pub mod filereader;
