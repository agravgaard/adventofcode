use std::fs::File;
use std::io::{BufRead, BufReader, Lines};
use std::path::Path;
pub fn read_filelines<P>(filepath: P) -> std::io::Result<Lines<BufReader<File>>>
where
    P: AsRef<Path>,
{
    let fp = File::open(filepath)?;
    let reader = BufReader::new(fp);

    Ok(reader.lines())
}

use std::str::FromStr;
pub fn filelines_to_vector<T>(filelines: Lines<BufReader<File>>) -> Vec<T>
where
    T: FromStr,
    <T as FromStr>::Err: std::fmt::Debug,
{
    let mut out: Vec<T> = Vec::new();
    for line in filelines {
        out.push(
            line.unwrap()
                .parse::<T>()
                .expect("Failed parsing value from line"),
        );
    }
    out
}
