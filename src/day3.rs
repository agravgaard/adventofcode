fn is_digit(c: char) -> bool {
    "1234567890".contains(c)
}

fn is_symbol(c: char) -> bool {
    !is_digit(c) && c != '.'
}

fn is_gear(c: char) -> bool {
    c == '*'
}

fn get_end_of_number(row: &String, i: usize) -> usize {
    let trim_right_index = row
        .chars()
        .enumerate()
        .skip(i)
        .find(|(_i, c)| !is_digit(*c));
    let (last_i, _) = trim_right_index.unwrap_or((row.len(), '.'));
    last_i
}

fn get_start_of_number(row: &String, i: usize) -> usize {
    let trim_left_index_rev = row
        .chars()
        .rev()
        .skip(row.len() - i)
        .enumerate()
        .find(|(_, c)| !is_digit(*c));
    let (first_i_rev, _) = trim_left_index_rev.unwrap_or((i, '.'));

    i - first_i_rev
}

fn get_number_at(row: &String, i: usize) -> u32 {
    if !is_digit(
        row.chars()
            .nth(i)
            .expect("bad index, nth char didn't exist"),
    ) {
        return 0;
    }
    let last_i = get_end_of_number(row, i);
    let first_i = get_start_of_number(row, i);

    let number: String = row.chars().skip(first_i).take(last_i - first_i).collect();
    number
        .parse::<u32>()
        .expect("number was not convertible to u32")
}

fn get_left_adjacent(i: usize, row: &String) -> u32 {
    if i >= 1 {
        return get_number_at(row, i - 1);
    }
    0
}

fn get_right_adjacent(i: usize, row: &String) -> u32 {
    if i < (row.len() - 1) {
        return get_number_at(row, i + 1);
    }
    0
}

fn get_left_right_adjacent_sum(row: &String, indices: &[usize]) -> u32 {
    indices.iter().fold(0, |acc, index| -> u32 {
        let i = *index;
        let mut sum = acc;
        if !is_digit(
            row.chars()
                .nth(i)
                .expect("bad index, nth char didn't exist"),
        ) {
            // check both left and right
            sum += get_left_adjacent(i, row);
            sum += get_right_adjacent(i, row);
        } else {
            // get number at i
            sum += get_number_at(row, i);
        }
        sum
    })
}

fn get_symbol_positions(row: &str) -> Vec<usize> {
    row.chars()
        .enumerate()
        .filter_map(|(i, c)| {
            if is_symbol(c) {
                return Some(i);
            }
            None
        })
        .collect()
}

fn get_adjacent_sum(current_row: &String, next_row: &String) -> u32 {
    // first, find symbols, i.e. not . and not number
    let current_symbols = get_symbol_positions(current_row);
    let next_symbols = get_symbol_positions(next_row);

    // check from current row left-right adjacent and down-diagonal
    let sum_current = get_left_right_adjacent_sum(current_row, &current_symbols);
    let sum_next = get_left_right_adjacent_sum(next_row, &current_symbols);
    // and from next row up-diagonal adjacent
    let sum_from_next = get_left_right_adjacent_sum(current_row, &next_symbols);

    sum_current + sum_next + sum_from_next
}

pub fn part_number_sum(v: &[String]) -> u32 {
    v.windows(2).fold(0, |acc, window| {
        acc + get_adjacent_sum(&window[0], &window[1])
    })
}

fn get_left_right_adjacent(row: &String, index: usize) -> Vec<(usize, u32)> {
    let mut output = Vec::<(usize, u32)>::new();
    if !is_digit(
        row.chars()
            .nth(index)
            .expect("bad index, nth char didn't exist"),
    ) {
        // check both left and right
        let left_adj = get_left_adjacent(index, row);
        if left_adj != 0 {
            output.push((index, left_adj));
        }
        let right_adj = get_right_adjacent(index, row);
        if right_adj != 0 {
            output.push((index, right_adj));
        }
    } else {
        // get number at i
        let number_at = get_number_at(row, index);
        if number_at != 0 {
            output.push((index, number_at));
        }
    }
    output
}

fn get_gear_positions(row: &str) -> Vec<usize> {
    row.chars()
        .enumerate()
        .filter_map(|(i, c)| {
            if is_gear(c) {
                return Some(i);
            }
            None
        })
        .collect()
}

fn get_adjacent_gears(prev_row: &String, current_row: &String, next_row: &String) -> u32 {
    // first, find gears, i.e. *
    let current_symbols = get_gear_positions(current_row);

    current_symbols.iter().fold(0, |acc, &index| {
        let prev = get_left_right_adjacent(prev_row, index);
        let current = get_left_right_adjacent(current_row, index);
        let next = get_left_right_adjacent(next_row, index);

        if prev.len() + current.len() + next.len() == 2 {
            return acc
                + prev.iter().fold(1, |acc, (_, v)| acc * v)
                    * current.iter().fold(1, |acc, (_, v)| acc * v)
                    * next.iter().fold(1, |acc, (_, v)| acc * v);
        }
        acc
    })
}

pub fn gear_sum(v: &[String]) -> u32 {
    let dummy = ".".repeat(v[0].len());
    let v_with_dummy = [
        vec![dummy.clone(), dummy.clone()],
        v.to_vec(),
        vec![dummy.clone(), dummy.clone()],
    ]
    .concat();
    v_with_dummy.windows(3).fold(0, |acc, window| {
        acc + get_adjacent_gears(&window[0], &window[1], &window[2])
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::filereader::{filelines_to_vector, read_filelines};

    #[test]
    fn test_get_number_at() {
        let input_row = String::from("467..778.1");
        let expected = [
            (0, 467),
            (1, 467),
            (2, 467),
            (3, 0),
            (4, 0),
            (5, 778),
            (6, 778),
            (7, 778),
            (8, 0),
            (9, 1),
        ];
        expected.iter().for_each(|(i, exp)| {
            let result = get_number_at(&input_row, *i);
            assert_eq!(result, *exp);
        });
    }

    #[test]
    fn test_part_number_sum() {
        let input = [
            String::from("467..114.."),
            String::from("...*......"),
            String::from("..35..633."),
            String::from("......#..."),
            String::from("617*......"),
            String::from(".....+.58."),
            String::from("..592....."),
            String::from("......755."),
            String::from("...$.*...."),
            String::from(".664.598.."),
        ];
        let result = part_number_sum(&input);
        assert_eq!(result, 4361);
    }

    #[test]
    fn test_part_number_sum_from_file() {
        let filelines = read_filelines("./data/day3.txt").expect("Failed reading lines, day3");
        let input = filelines_to_vector::<String>(filelines);
        let result = part_number_sum(&input);
        assert_eq!(result, 535078);
        // println!("Day 3, part 1: {}", result);
    }

    #[test]
    fn test_gear_sum() {
        let input = [
            String::from("467..114.."),
            String::from("...*......"),
            String::from("..35..633."),
            String::from("......#..."),
            String::from("617*......"),
            String::from(".....+.58."),
            String::from("..592....."),
            String::from("......755."),
            String::from("...$.*...."),
            String::from(".664.598.."),
        ];
        let result = gear_sum(&input);
        assert_eq!(result, 467835);
    }

    #[test]
    fn test_gear_sum_from_file() {
        let filelines = read_filelines("./data/day3.txt").expect("Failed reading lines, day3");
        let input = filelines_to_vector::<String>(filelines);
        let result = gear_sum(&input);
        assert_eq!(result, 75312571);
        // println!("Day 3, part 2: {}", result);
    }
}
